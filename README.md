# PIXI.js converter for animations

## Description

This tool will convert a given set of images into one sprite-image and the corresponding JSON file for using it inside of PIXI.js.

## Usage within your project

## Usage with the CLI

## Resources

* [NodeJS and ImageMagick](https://ciphertrick.com/2015/12/21/image-manipulation-using-nodejs-imagemagick/)
* [Testing with NodeJS and Jasmine](https://semaphoreci.com/community/tutorials/getting-started-with-node-js-and-jasmine)
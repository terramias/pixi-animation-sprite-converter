#!/usr/bin/env node

const program = require('commander')
const chalk = require('chalk')
const { version, name, description} = require('./package.json')
const { convertDirectoryToSprite } = require('./src/pixi-converter')

program
.version(version)
.description(description)
.arguments('<command>')
.option('-d, --destination <directory>', 'The username to authenticateThe destination directory for saving the image and JSON data to')
.option('-s, --source <directory>', 'The database-file to use', 'movies.db')
.option('-n, --name <name>', 'The name of the sprite that should be generated')
.action(command => {
  if (command === 'create') {
    console.log(chalk.green('Create a new sprite'))
    convertDirectoryToSprite({
    	source: program.source,
    	destination: program.destination,
    	options: {
    		name: program.name
    	}
    })
    return
  }
})
.parse(process.argv)

if (!process.argv.slice(2).length) {
  program.help()
}

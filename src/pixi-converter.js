var exports = module.exports = {}

const gm = require('gm')
const fs = require('fs') 
const path = require('path')

let basedir = ''

function identify(file) {
	return new Promise((resolve, reject) => {
		gm(file).
			antialias(false).
			identify((error, data) => {
				resolve(data)
			})
	})
}

function combineGif(file) {
	return new Promise((resolve, reject) => {
		let basedir = path.dirname(file)
		let basename = path.basename(file)
		let newfile = basedir + '/tmp/' + basename
		gm(file).
			antialias(false).
			dispose('previous').
			write(newfile, () => { 
				resolve(newfile)
			})
	})
}

function montageGif(file, data) {
	return new Promise((resolve, reject) => {
		let basedir = path.dirname(file)
		let basename = path.basename(file)
		let layers = []

		if(data.Geometry.length === undefined) {
			data.Geometry = [ data.Geometry ]
		}
		// create png-file
		data.Geometry.reduce((seq, n, i) => {
    	return seq.then(() => {
				return new Promise(resolve => {
					layers.push(file.replace('.gif' , '_' + i + '.png'))
					gm(file).
						antialias(false).
						selectFrame(i).
						write(file.replace('.gif' , '_' + i + '.png'), resolve)
				})
    	})
		}, Promise.resolve()).then(data => {
			return new Promise((resolve, reject) => {
				let file1 = layers.shift()
				let sprite = gm(file1).antialias(false)

				layers.forEach(layerfile => {
					sprite = sprite.append(layerfile, true)
				})
				let newfile = file.replace('.gif', '.png')
				console.log(file1)
				console.log(newfile)
				sprite.write(newfile, (err, data) => {
					fs.unlinkSync(file)
					fs.unlinkSync(file1)
					layers.forEach(file1 => {
						fs.unlinkSync(file1)
					})
				})
			})
		})
	})
}

function convertFilesToSprite(err, files) {
	let filelist = []
	
	files = files.map(function(file) {
		return path.resolve(basedir, file)
	})
	files = files.filter(function(file) {
		const stats = fs.statSync(file)
		
		if(file.indexOf('_combined') > 0) {
			return false
		}
		
		if(file.indexOf('.gif') === false) {
			return false
		}
		
		if(!stats.isFile()) {
			return false
		}

		return true
	})

	files.forEach(function(file) {
		let imageData
		let imageFile
		combineGif(file).then(file => {
			imageFile = file
			return identify(imageFile)
		}).then(data => {
			imageData = data
			return montageGif(imageFile, imageData)
		})

	})
}

exports.convertDirectoryToSprite = function( { source, destination, options } ) {
	let sourcefiles = []
	basedir = source
	if(!fs.existsSync(basedir + '/tmp')) {
		fs.mkdirSync(basedir + '/tmp')
	}
	fs.readdir(basedir, convertFilesToSprite)
}